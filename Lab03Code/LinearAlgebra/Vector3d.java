//Katharina Orfanidis 1842297
package LinearAlgebra;

public class Vector3d{

    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }
    //get methods for all fields
    public double getX(){
        return this.x;
    }
    
    public double getY(){
        return this.y;
    }
    public double getZ(){
        return this.z;
    }

    //calculates the magnitude of a vector
    public double magnitude(){
       return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2));
    }

    //calculates the dot product of two vectors
    public double dotProduct(Vector3d e){
        return (this.x * e.getX()) + (this.y * e.getY()) + (this.z * e.getZ());
    }

    //add to vectors together to make a new one
    public Vector3d add(Vector3d f){
        Vector3d g = new Vector3d(this.x + f.getX(), this.y + f.getY(), this.z + f.getZ());
        return g;
    }
}