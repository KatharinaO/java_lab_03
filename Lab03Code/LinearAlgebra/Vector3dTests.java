//Katharina Orfanidis 1842297
package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;


public class Vector3dTests {
    
    @Test
    public void testGet() {
        Vector3d e = new Vector3d(3, 4, 5);
        assertEquals(3, e.getX());
        assertEquals(4, e.getY());
        assertEquals(5, e.getZ());
    }
    @Test
    public void testMagnitude() {
        Vector3d e = new Vector3d(4, 4, 2);

        assertEquals(6, e.magnitude());
    }

    @Test
    public void testDotProduct() {
        Vector3d e = new Vector3d(1, 1, 2);
        Vector3d f = new Vector3d(1, 1, 2);

        assertEquals(6, e.dotProduct(f));
    }

    @Test
    public void testAdd() {
        Vector3d e = new Vector3d(1, 1, 2);
        Vector3d f = new Vector3d(1, 1, 2);

        assertEquals(2, e.add(f).getX());//fail test
        assertEquals(2, e.add(f).getY());
        assertEquals(4, e.add(f).getZ());
    }

}
